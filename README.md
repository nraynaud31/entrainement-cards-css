Reproduisez les cartes CSS suivantes

**Référentiels**

Développeur web et web mobile

**Ressource(s)**
[Card 1](https://simplonline-v3-prod.s3.eu-west-3.amazonaws.com/media/image/png/22125c8e-83c6-4231-a758-d54ecb656ce0.png)
[Card 2](https://simplonline-v3-prod.s3.eu-west-3.amazonaws.com/media/image/jpg/52c7bab3-3bff-4b29-b131-43a83a8099d5.jpg)
[Card 3](https://simplonline-v3-prod.s3.eu-west-3.amazonaws.com/media/image/jpg/80252154-7b71-40b0-aeff-7ad7f49f950c.jpg)

**Contexte du projet**

Vous devez reproduire les cartes css ci-jointes

**Modalités pédagogiques**

Travail à faire en solo.

Avec CSS, SCSS ou postCSS

**Livrables**

Un dépôt gitlab
